import React, { Component } from "react";
import { RowEmailAndLoginContainer } from "./components/NavBar/RowEmailAndLoginContainer";
import { SearchLogoRow } from "./components/NavBar/SearchLogoRow";
import { SearchOverlayMenu } from "./components/Search/SearchOverlayMenu";
import { SliderWrapper } from "./components/Slider/SliderWrapper";
import { SelectionTourWrapper } from "./components/Selection/SelectionTourWrapper";
import { CategoryTourWrapper } from "./components/Category/CategoryTourWrapper";
import { ServicesTourWrapper } from "./components/Services/ServicesTourWrapper";
import { FooterWrapper } from "./components/Footer/FooterWrapper";

class Home extends Component {

  componentDidMount() {
      window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
      window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
      let scrollTop = window.scrollY;
      if (scrollTop > 200){
        const x = document.getElementById('head');
        x.classList = "sticky";
      }
      if (scrollTop < 200){
        const x = document.getElementById('head');
        x.classList = "";
      }
  }

  render() {
    return (
      <div>
        <header id="head">
            <div id="top_line">
                <div className="container" id="DarkBarOnTopContainer"><RowEmailAndLoginContainer/></div>
            </div>
            <div id="search_logo_container" className="container bfont"><SearchLogoRow/></div>
        </header>

        <main id="mainContainer">
            <div id="full-slider-wrapper"><SliderWrapper/></div>
            <div id="selection-tour" className="container margin_60"><SelectionTourWrapper/></div>
            <div id="white_bg" className="white_bg"><CategoryTourWrapper/></div>
            <div id="services" className="container margin_60"><ServicesTourWrapper/></div>
        </main>
        <footer id="footer" className="revealed"><FooterWrapper/></footer>
        <div className="search-overlay-menu" id="search-overlay-menu"><SearchOverlayMenu/></div>
      </div>
    );
  }
}
export default Home;
