import React from "react";
import loading from "../.././img/loading.gif"

const createElement = React.createElement;

export class LoadingIcon extends React.Component {

    render() {
        if (!this.props.showLoading) {
            const icon = createElement('img', {className:'notloadingimage', src: loading}, );
            return icon;
        } else {
            const icon = createElement('img', {className:'loadingimage', src: loading}, );
            return icon;
        }
    }

}
