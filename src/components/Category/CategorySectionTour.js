import React from "react";
import {CategoryTour} from "./CategoryTour";
import { Col } from 'reactstrap';

const createElement = React.createElement;

export class CategorySectionTour extends React.Component {
    render() {
        const a = createElement('ul',{},
            this.props.dataitems.map((item,i) => (
                <CategoryTour key={i} href_link={item.hreflink} name_text={item.nametext} classicon={item.classicon}/>
            )));
        return createElement(Col,{xs: "6", md: "3", className: "other_tours",}, a);
    }
}
