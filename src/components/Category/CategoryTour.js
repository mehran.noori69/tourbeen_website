import React from "react";
import {NavLink} from "react-router-dom";

const createElement = React.createElement;
export class CategoryTour extends React.Component {
    render() {
        const a = createElement(NavLink,{to: this.props.href_link, },
            this.props.name_text, createElement('i',{className: this.props.classicon},));
        return createElement('li',{className: "text-right",}, a);
    }
}
