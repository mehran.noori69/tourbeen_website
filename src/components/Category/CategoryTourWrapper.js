import React from "react";
import {CategorySectionTour} from "./CategorySectionTour";
import 'font-awesome/css/font-awesome.min.css';

const createElement = React.createElement;

export class CategoryTourWrapper extends React.Component {
    render() {
        const x = createElement('div',{className: "main_title bfont",},
            createElement('h2', {}, " دسته بندی تورهای گردشگری بر اساس شهرها"));
        const firstItems = [{ hreflink: '/tours/?city=آنتالیا', classicon: 'fa fa-hotel', nametext: 'آنتالیا'},
            { hreflink: '/tours/?city=باکو', classicon: 'fa fa-compass', nametext: 'باکو'},
            { hreflink: '/tours/?city=بدروم', classicon: 'fa fa-shower', nametext: 'بدروم'},
            { hreflink: '/tours/?city=پاتایا', classicon: 'fa fa-compass', nametext: 'پاتایا'},
            { hreflink: '/tours/?city=بالی', classicon: 'fa fa-compass', nametext: 'بالی'}];
        const secondItems = [{ hreflink: '/tours/?city=پکن', classicon: 'fa fa-home', nametext: 'پکن'},
            { hreflink: '/tours/?city=استانبول', classicon: 'fa fa-wifi', nametext: 'استانبول'},
            { hreflink: '/tours/?city=آگرا', classicon: 'fa fa-hotel', nametext: 'آگرا'},
            { hreflink: '/tours/?city=تفلیس', classicon: 'fa fa-compass', nametext: 'تفلیس'},
            { hreflink: '/tours/?city=بانکوک', classicon: 'fa fa-shower', nametext: 'بانکوک'}];
        const thirdItems = [{ hreflink: '/tours/?city=دهلی', classicon: 'fa fa-compass', nametext: 'دهلی'},
            { hreflink: '/tours/?city=کوشی آداسی', classicon: 'fa fa-compass', nametext: 'کوشی آداسی'},
            { hreflink: '/tours/?city=مارماریس', classicon: 'fa fa-home', nametext: 'مارماریس'},
            { hreflink: '/tours/?city=کوالالامپور', classicon: 'fa fa-wifi', nametext: 'کوالالامپور'},
            { hreflink: '/tours/?city=مسکو', classicon: 'fa fa-hotel', nametext: 'مسکو'}];
        const fourthItems = [{ hreflink: '/tours/?city=جیپور', classicon: 'fa fa-compass', nametext: 'جیپور'},
            { hreflink: '/tours/?city=شانگهای', classicon: 'fa fa-shower', nametext: 'شانگهای'},
            { hreflink: '/tours/?city=دبی', classicon: 'fa fa-compass', nametext: 'دبی'},
            { hreflink: '/tours/?city=پوکت', classicon: 'fa fa-compass', nametext: 'پوکت'},
            { hreflink: '/tours/?city=کیش', classicon: 'fa fa-home', nametext: 'کیش'}];
        const y = createElement('div',{className: "row add_bottom_45 bfont",},
            <CategorySectionTour dataitems={firstItems}/>,
            <CategorySectionTour dataitems={secondItems}/>,
            <CategorySectionTour dataitems={thirdItems}/>,
            <CategorySectionTour dataitems={fourthItems}/>
        );
        return createElement('div',{className: "container margin_60"}, x,y);
    }
}
