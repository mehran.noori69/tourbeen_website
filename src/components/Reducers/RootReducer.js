import { combineReducers } from "redux";
import tours from "./ToursReducer";

export default combineReducers({
  tours
});
