export const Fetch_Tours_Begin   = 'Fetch_Tours_Begin';
export const Fetch_Tours_Success = 'Fetch_Tours_Success';
export const Fetch_Tours_Failure = 'Fetch_Tours_Failure';

export const fetchToursBegin = () => ({
  type: Fetch_Tours_Begin
});

export const fetchToursSuccess = tours => ({
  type: Fetch_Tours_Success,
  payload: { tours }
});

export const fetchToursFailure = error => ({
  type: Fetch_Tours_Failure,
  payload: { error }
});

export function fetchTours(country, city, query, nights, agancy, page) {
  return dispatch => {
    let formData = new FormData();
    formData.append('city', city);
    formData.append('country', country);
    formData.append('q', query);
    formData.append('nights', nights);
    formData.append('agancy', agancy);
    formData.append('page', page);
    dispatch(fetchToursBegin());
    return fetch("https://gooshichand.com/tours/gettouritems/", {method: 'POST', body: formData})
      .then(handleErrors)
      .then(res => res.json())
      .then(json => {
        dispatch(fetchToursSuccess(json));
        return json;
      })
      .catch(error => dispatch(fetchToursFailure(error)));
  };
}

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
