import {
  Fetch_Tours_Begin,
  Fetch_Tours_Success,
  Fetch_Tours_Failure
} from './TourActions.js';

const initialState = {
  items: [],
  loading: false,
  error: null
};

export default function ToursReducer(state = initialState, action) {
  switch(action.type) {
    case Fetch_Tours_Begin:
      return {
        ...state,
        loading: true,
        error: null
      };
    case Fetch_Tours_Success:
      return {
        ...state,
        loading: false,
        items: action.payload.tours
      };
    case Fetch_Tours_Failure:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: []
      };
    default:
      return state;
  }
}
