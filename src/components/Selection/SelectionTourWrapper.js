import React from "react";
import {SelectionTour} from "./SelectionTour";
import teblisi from "../.././img/teblisi.jpg"
import antaliya from "../.././img/antaliya.jpg"
import pekan from "../.././img/pekan.jpg"
import {NavLink} from "react-router-dom";

const createElement = React.createElement;

export class SelectionTourWrapper extends React.Component {
    render() {
        const x = createElement('div',{className: "main_title bfont",},
            createElement('h2', {}, "تورهای منتخب"));
        const y = createElement('div',{className: "row bfont",},
            <SelectionTour link_text="/tours/?city=تفلیس" src_text={teblisi} img_name="تفلیس" title_text="تورهای تفلیس" head_text="پرطرفدار"/>,
            <SelectionTour link_text="/tours/?city=آنتالیا" src_text={antaliya} img_name="آنتالیا" title_text="تورهای آنتالیا" head_text="پرطرفدار"/>,
            <SelectionTour link_text="/tours/?city=پکن" src_text={pekan} img_name="پکن" title_text="تورهای پکن" head_text="پرطرفدار"/>
        );
        const z = createElement('p',{className: "text-center add_bottom_30 bfont",},
            createElement(NavLink, {to: "/tours/", className: "btn_1 medium"},
                createElement('i',{ className: "icon-eye-7"},""), "مشاهده همه تورها"));
        return createElement('div',{}, x,y,z);
    }
}
