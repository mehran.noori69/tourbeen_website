import React from "react";
import {NavLink} from "react-router-dom";

const createElement = React.createElement;
export class SelectionTour extends React.Component {
    render() {
        const b = createElement('div',{className: "ribbon_3 popular", },
            createElement('span',{}, this.props.head_text));
        const c = createElement('div',{className: "img_container", },
            createElement(NavLink,{to: this.props.link_text},
                createElement('img', {src: this.props.src_text, className: "img-responsive", alt: this.props.img_name},),));
        const d = createElement('div',{className: "tour_title", },
            createElement('h3',{className: "text-right"}, this.props.title_text));
        const a = createElement('div',{className: "tour_container", }, b, c, d);
        return createElement('div',{className: "col-md-4 col-sm-6 wow zoomIn", 'data-wow-delay': "0.1s"}, a);
    }
}
