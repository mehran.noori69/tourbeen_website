import React from "react";
import laptop from "../.././img/12.jpg"
import {NavLink} from "react-router-dom";
import { Row, Col } from 'reactstrap';


const createElement = React.createElement;
export class ServicesTourWrapper extends React.Component {
    render() {
        const x = createElement(Col,{md: "8", className: "hidden-xs",},
            createElement('img', {src: laptop, alt: "Laptop", className: "img-responsive laptop"}, ));
        const y = createElement(Col,{md: "4", className: "text-right bfont",},
            createElement('h3', {}, "خدمات وب سایت",
                createElement('span',{}," توربین")),
            createElement('ul', {className: "list_order"},
                createElement('li',{},'مشاهده تورهای آژانس های مختلف در یک قاب'),
                createElement('li',{},'دسته بندی تورها بر اساس کشور، شهر، آژانس، زمان شروع، قیمت و'),
                createElement('li',{},'یافتن ارزان ترین و مطمئن ترین تورهای گردشگری')
            ),
            createElement(NavLink, {to: "/tours/", className: "btn_1"}, "شروع")
        );
        return createElement(Row,{}, x, y);
    }
}
