import React from "react";
import {FooterCountry} from "./FooterCountry";
import 'font-awesome/css/font-awesome.min.css';

const createElement = React.createElement;

export class FooterWrapper extends React.Component {
    render() {
        const firstItems = [{ hreflink: '/tours/?country=چین', nametext: 'چین'},
            { hreflink: '/tours/?country=اذربایجان', nametext: 'اذربایجان'},
            { hreflink: '/tours/?country=مالدیو', nametext: 'مالدیو'},
            { hreflink: '/tours/?country=مالزی', nametext: 'مالزی'}];
        const secondItems = [{ hreflink: '/tours/?country=تایلند', nametext: 'تایلند'},
            { hreflink: '/tours/?country=ترکیه', nametext: 'ترکیه'},
            { hreflink: '/tours/?country=روسیه', nametext: 'روسیه'},
            { hreflink: '/tours/?country=ارمنستان', nametext: 'ارمنستان'}];
        const thirdItems = [{ hreflink: '/tours/?country=امارات', nametext: 'امارات'},
            { hreflink: '/tours/?country=فرانسه', nametext: 'فرانسه'},
            { hreflink: '/tours/?country=قبرس', nametext: 'قبرس'},
            { hreflink: '/tours/?country=گرجستان', nametext: 'گرجستان'}];
        const fourthItems = [{ hreflink: '/tours/?country=ایران', nametext: 'ایران'},
            { hreflink: '/tours/?country=اندونزی', nametext: 'اندونزی'},
            { hreflink: '/tours/?country=سنگاپور', nametext: 'سنگاپور'},
            { hreflink: '/tours/?country=موریس', nametext: 'موریس'}];
        let footer_country = ["footer_country_one", "footer_country_two", "footer_country_three", "footer_country_four"];
        const x = createElement('div',{className: "row bfont text-right",},
            createElement('div', {className: "col-md-3 col-sm-3"},
                createElement('h3',{},"تماس با ما؟"),
                createElement('a',{id: "phone", href: "tel://9802112345678", className: "has-number"},"9802112345678"),
                createElement('a',{href: "mailto://info@tourbeen.ir", id: "email_footer"},"info at tourbeen dot ir")
            ),
            <FooterCountry dataitems={firstItems} ulkey={footer_country[0]}/>,
            <FooterCountry dataitems={secondItems} ulkey={footer_country[1]}/>,
            <FooterCountry dataitems={thirdItems} ulkey={footer_country[2]}/>,
            <FooterCountry dataitems={fourthItems} ulkey={footer_country[3]}/>
        );
        const y = createElement('div',{className: "row",},
            createElement('div',{className: "col-md-12"},
                createElement('div',{id:"social_footer"},
                    createElement('ul',{},
                        createElement('li',{},
                            createElement('a',{href: "#"},
                                createElement('i',{className: "fa fa-facebook"},)
                            )),
                        createElement('li',{},
                            createElement('a',{href: "https://t.me/daily_tour"},
                                createElement('i',{className: "fa fa-twitter"},)
                            )),
                        createElement('li',{},
                            createElement('a',{href: "#"},
                                createElement('i',{className: "fa fa-instagram"},)
                            )),
                        createElement('li',{},
                            createElement('a',{href: "#"},
                                createElement('i',{className: "fa fa-linkedin"},)
                            ))
                    ))));
        return createElement('div',{className: "container"}, x, y);
    }
}
