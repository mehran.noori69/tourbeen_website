import React from "react";
import {NavLink} from "react-router-dom";
import { Col } from 'reactstrap';

const createElement = React.createElement;
export class FooterCountry extends React.Component {
    constructor(props){
        super(props);
        this.state = { ikey: 0 };
    }
    render(){
        const ulelement = createElement('ul',{key: this.props.ulkey},
            this.props.dataitems.map((item, id) => (
                createElement('li',{key: id},
                    createElement(NavLink,{to: item.hreflink,},item.nametext)
                )
            ))
        );
        return createElement(Col, {sm: "2", xs: "6"}, ulelement)
    }
}
