import React from "react";
import {SearchOverlayMenuForm} from "./SearchOverlayMenuForm";
import 'font-awesome/css/font-awesome.min.css';


const createElement = React.createElement;

export class SearchOverlayMenu extends React.Component {

    removeOpenClass(e) {
        e.preventDefault();
        const x = document.getElementById('search-overlay-menu');
        x.className = "search-overlay-menu";
    }
    render() {
        const search_overlay_i = createElement('i',{className: "fa fa-close"}, );
        const search_overlay_close = createElement('span',{className: "search-overlay-close",
         onClick: this.removeOpenClass}, search_overlay_i);
        return createElement('section', {}, search_overlay_close, <SearchOverlayMenuForm/>)
    }
}
