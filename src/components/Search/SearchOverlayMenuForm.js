import React from "react";
import 'font-awesome/css/font-awesome.min.css';
import { Link } from 'react-router-dom';

export class SearchOverlayMenuForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        query: '',
    };
    this.handleChangeInput = this.handleChangeInput.bind(this);
  }
  handleChangeInput() {
      let query = this.refs.query.value;
      console.log(query);
      this.setState({
        query: query
      })
  }
  render() {
    return (
      <div className="text-right">
        <Link id="search_link" to={`/tours/?q=${this.state.query}`}>
          <button>
            <i className="fa fa-search fasize"></i>
          </button>
        </Link>
        <input ref="query" id="query" onChange={this.handleChangeInput} className="bfont text-right"
           name="q" type="search" placeholder="جستجو در تورهای گردشگری"/>
      </div>
    );
  }
}

export default SearchOverlayMenuForm
