import React from "react";
import {TourItem} from './TourItem'
import {LoadingIcon} from '.././Loading/Loading'
import { Container, Row } from 'reactstrap';

import { connect } from "react-redux";
import { fetchTours } from ".././Reducers/TourActions";
import { withRouter } from "react-router-dom";
const createElement = React.createElement;
const queryString = require('query-string');

class TourItemWrapper extends React.Component {

    componentWillReceiveProps(){
        this.currenturl = window.location.href;
        let equal = this.preurl === this.currenturl
        if(!equal){
          this.preurl = window.location.href;
          this.serviceCall();
        }
        if(equal){
          return false;
        }else {
          return true;
        }
    }

    componentDidMount() {
        this.preurl = window.location.href;
        this.currenturl = window.location.href;
        this.serviceCall();
    }

    serviceCall(){
      const parsed = queryString.parse(this.props.history.location.search);
      let country = '', city = '', query = '', nights = '', agancy = '', page = 0;
      country = parsed.country ? parsed.country : ''
      city = parsed.city ? parsed.city : ''
      query = parsed.query ? parsed.query : ''
      nights = parsed.nights ? parsed.nights : ''
      agancy = parsed.agancy ? parsed.agancy : ''
      page = parsed.page ? parsed.page : 0
      this.props.dispatch(fetchTours(country, city, query, nights, agancy, page));
    }

    render() {
        const { loading, items } = this.props;
        const x = createElement('div',{className: "main_title bfont",},
            createElement('p',{className: "bfont",},'لیست تورها'),
        );
        var ulelement = ''
        if (!loading) {
          ulelement = createElement(Row, {id: "ul-tour"},
              items.map((item, i) => (
                  <TourItem key={i} title_name={item.title} price_name={item.price} period_name={item.nights} airline_name={item.airline}
                            date_name={item.start} hotel_name={item.hotelname} agency_name={item.agancy_name} agency_phone={item.agancy_phone}/>
              ))
          );
        }
        if (loading) {
          return createElement(Container, {}, x, ulelement, <LoadingIcon showLoading={true}/>);
        }else{
          return createElement(Container, {}, x, ulelement, <LoadingIcon showLoading={false}/>);
        }
    }

}

function mapStateToProps(state) {
  return {
    items: state.tours.items,
    loading: state.tours.loading,
    error: state.tours.error
  }
};

// You should export this and not export TourItemWrapper
export default withRouter(connect(mapStateToProps)(TourItemWrapper));
