import React from "react";
import { Col } from 'reactstrap';

const createElement = React.createElement;

export class TourItem extends React.Component {
    render(){
        const title = createElement('h5',{className: "bfont text-center has-number tour-title"}, ToFaNumber(this.props.title_name));
        const hr = createElement('hr',{}, );
        const price = createElement('div',{className: "bfont text-right has-number commafy"}, "قیمت: ", ToFaNumber(this.props.price_name));
        const period = createElement('div',{className: "bfont text-right has-number"}, "مدت: ", ToFaNumber(this.props.period_name) + " شب");
        const airline = createElement('div',{className: "bfont text-right"}, "ایرلاین: ", this.props.airline_name);
        const date = createElement('div',{className: "bfont text-right has-number"}, ToFaNumber(this.props.date_name));
        const hotel = createElement('div',{className: "bfont text-right"}, "هتل: ", this.props.hotel_name);
        const agency = createElement('div',{className: "agancybox"},
            createElement('div',{className: "bfont text-right has-number"}, this.props.agency_name),
            createElement('div',{className: "bfont text-right has-number"}, ToFaNumber(this.props.agency_phone))
        );

        const a = createElement('div',{className: "white_bg touritem"}, title, hr, date, price, period, airline, hotel, agency);
        return createElement(Col,{sm: "6", md: "4"},a)
    }
}

function ToFaNumber(x){
    let arabicNumbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    let chars = x.split('');
    for (let i = 0; i < chars.length; i++) {
        if (/\d/.test(chars[i])) {
            chars[i] = arabicNumbers[chars[i]];
        }
    }
    return chars.join('');
}
