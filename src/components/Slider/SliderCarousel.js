import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

export class SlideCarousel extends React.Component {
    render() {
        return (
            <Carousel className="bfont" showThumbs={false} autoPlay={true} showIndicators={true} infiniteLoop={true} transitionTime={100}>
                <div>
                    <img src={this.props.slide_1} alt="" />
                    <p className="legend">بیش از 100 تور - 30 شهر در 20 کشور</p>
                </div>
                <div>
                    <img src={this.props.slide_2} alt="" />
                    <p className="legend">مقایسه خدمات هتل - ارزان ترین قیمت</p>
                </div>
                <div>
                    <img src={this.props.slide_3} alt="" />
                    <p className="legend">بیش از 100 تور - 30 شهر در 20 کشور</p>
                </div>
            </Carousel>
        );
    }
}
