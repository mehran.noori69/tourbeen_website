import React from "react";
import {SlideCarousel} from "./SliderCarousel";

import slide_1 from "../.././img/slide_1.jpg"
import slide_2 from "../.././img/slide_2.jpg"
import slide_3 from "../.././img/slide_3.jpg"

const createElement = React.createElement;

export class SliderWrapper extends React.Component {
    render() {
        return createElement('div',{},<SlideCarousel slide_1={slide_1} slide_2={slide_2} slide_3={slide_3}/>);
    }
}
