import React from "react";
import { Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import { Col } from 'reactstrap';

export class LoginMenuContainer extends React.Component {

    constructor(props) {
      super(props);
      this.toggle = this.toggle.bind(this);
      this.state = {
        dropdownOpen: false
      };
    }

    toggle() {
      this.setState(prevState => ({
        dropdownOpen: !prevState.dropdownOpen
      }));
    }

    render() {
        return (
          <Col sm="6" xs="6" md="6" className="bfont">
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <div id="LoginMenuContainer">
                <ul id="top_links">
                  <li>
                    <div className="dropdown dropdown-access">
                    <DropdownToggle className="dropdown-toggle bfont">ورود</DropdownToggle>
                      <DropdownMenu>
                        <div className="form-group">
                          <input className="form-control" type="text" id="inputUsernameEmail" placeholder="Email"/>
                        </div>
                        <div className="form-group">
                          <input className="form-control" type="password" id="inputPassword" placeholder="Password"/>
                        </div>
                        <a className="bfont text-right" id="forgot_pw" href="/">فراموشی رمزعبور؟</a>
                        <input type="submit" name="Sign_up" className="button_drop outline bfont" id="Sign_up" value="ثبت نام"/>
                        <input type="submit" name="Sign_in" className="button_drop outline bfont" id="Sign_in" value="ورود"/>
                      </DropdownMenu>
                    </div>
                  </li>
                </ul>
              </div>
            </Dropdown>
          </Col>
        );
    }
}
