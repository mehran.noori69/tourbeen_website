import React from "react";
import 'font-awesome/css/font-awesome.min.css';
import { Col } from 'reactstrap';

const createElement = React.createElement;

export class SearchIconDiv extends React.Component {
    addOpenClass(e) {
        e.preventDefault();
        const x = document.getElementById('search-overlay-menu');
        x.className = "search-overlay-menu open";
    }
    render() {
        const top_tools_i = createElement('i',{className: "fa fa-search"}, );
        const top_tools_a = createElement('a',{className: "search-overlay-menu-btn", 'datatoggle': "dropdown", href: "#", onClick: this.addOpenClass}, top_tools_i);
        const top_tools_div = createElement('div',{className: "dropdown dropdown-search"}, top_tools_a);
        const top_tools_li = createElement('li',{}, top_tools_div);
        const top_tools = createElement('ul',{id: "top_tools"}, top_tools_li);
        return createElement(Col,{sm: "9", xs: "9", md: "9"}, top_tools)
    }
}
