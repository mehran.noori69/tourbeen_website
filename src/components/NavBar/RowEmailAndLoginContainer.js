import React from "react";
import {PhoneIconOnTop} from "./PhoneIconOnTop";
import {LoginMenuContainer} from "./LoginMenuContainer";
import { Row } from 'reactstrap';

const createElement = React.createElement;
export class RowEmailAndLoginContainer extends React.Component {
    render() {
        return createElement(Row,{}, <PhoneIconOnTop/>, <LoginMenuContainer/>)
    }
}
