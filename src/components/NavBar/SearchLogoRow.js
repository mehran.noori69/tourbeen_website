import React from "react";
import {SearchIconDiv} from "./SearchIconDiv";
import {LogoDiv} from "./LogoDiv";

const createElement = React.createElement;

export class SearchLogoRow extends React.Component {
    render() {
        return createElement('div',{className: "row"}, <LogoDiv/>, <SearchIconDiv/>)
    }
}
