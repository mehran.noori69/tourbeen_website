import React from "react";
import { Col } from 'reactstrap';

const createElement = React.createElement;
export class PhoneIconOnTop extends React.Component {
    render() {
        const iconphone = createElement('i',{className: "icon-phone"}, '');
        const email = createElement('strong',{},'info at tourbeen.ir');
        return createElement(Col,{sm: "6", xs: "6", id: 'PhoneIconOnDarkBar'},iconphone,email)
    }
}
