import React from "react";
import {NavLink} from "react-router-dom";
import { Col } from 'reactstrap';

const createElement = React.createElement;
export class LogoDiv extends React.Component {
    render() {
        const logo_home_a = createElement(NavLink,{title: "", to: "/"}, 'Tourbeen');
        const logo_home_h1 = createElement('h1',{}, logo_home_a);
        const logo_home = createElement('div',{id: "logo_home"}, logo_home_h1);
        return createElement(Col,{sm: "3", xs: "3", md: "3"}, logo_home)
    }
}
